---
wip: true
sigle: INF5153
title: 'Génie logiciel: conception'
session: Automne 2023
encart-aide: true
resp:
  nom: Mili
  prenom: Hafedh
  local: PK-4340
  tel: poste 3943
  matricule: MILH5
  job: Professeur
  email: mili.hafedh@uqam.ca
  web: https://professeurs.uqam.ca/professeur/mili.hafedh/
ens:
  nom: Khazri
  prenom: Saida
  local: PK-4115
  matricule: KHASG
  job: Chargée de cours
  email: khazri.saida@uqam.ca
  web: https://www.linkedin.com/in/saida-khazri-91752229/
---

<!-- DESCRIPTION OFFICIELLE DU COURS INSÉRÉE AUTOMATIQUEMENT -->

<!-- VOTRE APPROCHE PÉDAGOGIQUE PLUS BAS -->

[Site du cours](https://www.moodle2.uqam.ca/coursv3/course/view.php?id=41805)

## Objectifs du cours

-   Caractériser les  propriétés d'une bonne conception.
-   Définir les principes sous-jacents des approches orientée objet.
-   Initier les étudiants à la notation UML (classe, séquence, états).
-   Rendre les étudiants aptes à réaliser des modèles de conception orientés objet originaux
-   Faire apprécier aux étudiants l'importance des enjeux reliés à la conception.
-   Familiariser les étudiants aux principaux patrons utilisés pour la conception orientée objet.
-   Rendre les étudiants aptes à réaliser un document de conception justifiant celle-ci.
-   Rendre les étudiants capables de comparer des conceptions, et de les caractériser    
-   Initier les étudiants aux nouvelles approches dans le domaine.

# Contenu 

1. Thème 1: Introduction.
    -   Place de la conception dans le cycle de vie du logiciel
    -   De programmeur à développeur
    -   Rappels de modélisation (modélisation de processus, cas d'utilisation, diagramme de classes, diagramme de séquence, etc.)
2. Thème 2: Concepts de base de la programmation objet
    -   Encapsulation et dissimulation de l'information
    -   Types & interfaces
    -   Héritage, classes abstraites & interfaces
    -   La relation de composition
3. Thème 3: Conception & Attribution des Responsabilités
    -   Retour sur la responsabilité unique
    -   Les principes GRASP
    -   Introduction à la `SOLID`ité du code
4. Thème 4: Conception architecturale
    -   Modèles architecturaux (Architecture client-serveur, SOA,Architecture microservices, MVC)
    -   Les principes SOLID (Responsabilité Unique, Ouverture/Fermeture, Substitution de Liskov, Interface Segregation, Inversion de Dépendance)
    -   Architectures distribuées
5. Thème 5: Introduction aux patrons de conception
    -   Pourquoi des patrons de conception ?
    -   Types de Patrons de Conception
    -   Critères de Choix des Patrons de Conception
6. Thème 6: Les patrons de conception
    -   Patrons de Conception Création
    -   Patrons de Conception Structure
    -   Patrons de Conception Comportement
    -   Mises en Garde et Anti-patrons
7. Thème 7: Sujets spéciaux
    -   Test-Driven Development (TDD)
    -   Architecture micro-services
    -   DevOps
 
<br/>

## Modalités d'évaluation


  Description sommaire      | Date             | Pondération
  ----------------------    | ------           | -------------
  Examen Intra              | 26 Octobre 2023  |  20% |
  Examen Final              | 14 Décembre 2023 |  30% |
  TP1                       | 12 Octobre 2023  |  10% |
  TP2                       | 9 Novembre 2023  |  20% |
  TP3                       | 7 Décembre 2023  |  20% |
 

# Renseignements utiles

Le cours INF5151 est un préalable à ce cours. Particulièrement, il est
important que les étudiants sachent lire un diagramme de classes et un diagramme de séquences.

Bien qu'un bref rappel de modélisation sera présenté, il
est fortement recommandé aux étudiants de rafraîchir leurs connaissances
en modélisation en vue de suivre ce cours.

# Médiagraphie

VO -   Introduction to Software Design with Java, 2019. Martin Robillard.

VC - UML@Classroom, Springer Verlag, 2015. Martina Seidl, Marion Scholz,
Christian Huemer & Gerti Kappel.

VR - Clean Code. Robert Martin. Prentice Hall.

VR- Clean Architecture: A Craftsman's Guide to Software Structure and Design.Robert Martin. Prentice Hall.

VO- Les Design Patterns en Java : les 23 modèles de conception fondamentaux.Metsker, S. J., & Wake, W. C. (2005). Pearson Education.

VR - Head First Design Patterns, a brain friendly guide. Eric Freeman, Elisabeth
Robson. O'REILLY.

VC - Software Systems Architecture: Working With Stakeholders Using
Viewpoints and Perspectives, Addison Wesley, 2011. Nick Rozanski & Eoin
(pronounced “Owen”) Woods.

VC - Pragmatic Unit Testing. Jeff Langr, with Andy Hunt and Dave Thomas. The
pragmatic programmers.

VC - Design Patterns: Elements of Reusable Object-Oriented Software,
Addison Wesley, 1994. Erich Gamma, Richard Helm, Ralph Johnson & John
Vlissides.

VC - Refactoring: Improving the Design of Existing Code, Addison Wesley,
2018 (2nd edition) Martin Fowler.

Légende: A: article - C: comptes rendus - L: logiciel - S: standard - U
: url - V: volume

C: complémentaire - O: obligatoire - R: recommandé
